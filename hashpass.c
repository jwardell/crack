#include <stdio.h>
#include <string.h>
#include "md5.h"
#include <stdlib.h>

int main( int argc, char *argv[] )
{
    if (argc != 3)
    {
        printf( "USAGE: password_file_to_hash output_file\n");
        exit(1);
    }
    FILE *output;
    FILE *input;

    output = fopen(argv[2], "w");
    input = fopen(argv[1], "r");
    
    if (!input)
    {
        printf("ERROR: Can't open %s for reading", argv[2]);
        exit(2);
    }
    
    if (!output)
    {
        printf("ERROR: Can't open %s for writing", argv[2]);
        exit(3);
    }
    
    char line[100];
    while( fgets( line, 100, input) ) // Reads input file line by line
    {
        for (int i = 0; i < strlen(line); i++)
        {
            if( line[i] == '\n' ) // check and deletes newline characters on each line
            {
                line[i] = '\0';
            }
        }
        char *hash = md5( line, strlen(line) ); // hashes password
        fprintf(output, "%s\n", hash);  // writes hash into output file
        free(hash);
    }
}